import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';
import { validationResult } from 'express-validator';
import User, { UserDocument } from '../models/user';
import { generateToken } from '../middleware/authMiddleware';

export const registerUser = async (req: Request, res: Response) => {
  try {
    // Perform validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.error('Validation failed', 400, errors);
    }

    const { username, email, password, role } = req.body;

    // Check if user with the given email already exists
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.error('User with this email already exists', 400);
    }

    // Hash the password
    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password, salt);

    // Determine the role
    const userRole = role || 'user'; // Default to 'user' if role is not provided or invalid

    // Create new user
    const newUser: UserDocument = await User.create({ username, email, password: passwordHash, role: userRole });

    res.success(newUser);
  } catch (error) {
    console.error(error);
    res.error('Server error');
  }
};

export const loginUser = async (req: Request, res: Response) => {
    try {
      // Validate input
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.error('Validation failed', 400, errors.array());
      }
  
      const { email, password } = req.body;
  
      // Check if user with the given email exists
      const user = await User.findOne({ email });

      
      if (!user) {
        return res.error('Invalid email or password', 401);
      }
  
      // Check if the provided password is correct
      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) {
        return res.error('Invalid email or password', 401);
      }
   
       // Generate JWT token with user data
       const token = generateToken({ userId: user._id, username: user.username, role: user.role });

       const userData = {
           _id: user._id,
           username: user.username,
           email: user.email,
           role: user.role
       };

       res.success({ user: userData, token });
    
    } catch (error) {
      console.error(error);
      res.error('Server error');
    }
};
 