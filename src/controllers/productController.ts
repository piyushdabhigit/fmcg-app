import { validationResult } from 'express-validator';
import mongoose from 'mongoose';
import ProductModel, { ProductDocument } from '../models/product';
import CategoryModel from '../models/category';
import { Request, Response } from 'express';

export const createProduct = async (req: Request, res: Response) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.error('Validation failed', 400, errors.array());
    }

    // Extract product data from request body
    const { name, description, price, category, upc } = req.body;

    // Check if the category ID is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(category)) {
      return res.error('Invalid category ID', 400);
    }

    // Check if the category exists in the Category table
    const existingCategory = await CategoryModel.findById(category);
    if (!existingCategory) {
      return res.error('Category does not exist', 400);
    }

    // Check if the UPC already exists
    const existingProductWithUPC = await ProductModel.findOne({ upc });
    if (existingProductWithUPC) {
      return res.error('Product with this UPC already exists', 400);
    }

    // Create a new product document
    const newProductData = { name, description, price, category, upc };
    const newProductInstance = new ProductModel(newProductData);

    // Save the new product to the database
    const savedProduct: ProductDocument = await newProductInstance.save();

    // Respond with success and created product data
    res.success(savedProduct, 'Product created successfully', 201);
  } catch (error) {
    console.error(error);
    res.error('Server error', 500);
  }
};

export const getProduct = async (req: Request, res: Response) => {
  try {
    // Validate request parameters
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.error('Validation failed', 400, errors.array());
    }

    // Extract filters from query parameters
    const { id, name, upc, categoryName } = req.query;

    // Construct filter object based on provided query parameters
    const filter: any = {};
    if (id) filter._id = id; // Search by product ID
    if (name) filter.name = { $regex: new RegExp(name as string, 'i') }; // Case-insensitive search for name
    if (upc) filter.upc = upc;
    if (categoryName) {
      // Get category ID by name
      const category = await CategoryModel.findOne({ name: categoryName as string });
      if (category) {
        filter.category = category._id;
      } else {
        // If category name does not exist, return empty result
        return res.success({ products: [], totalPages: 0, currentPage: 1 }, 'No products found',404);
      }
    }

    // Pagination parameters
    const page: number = parseInt(req.query.page as string, 10) || 1;
    const limit: number = parseInt(req.query.limit as string, 10) || 10; // Default limit is 10
    const skip: number = (page - 1) * limit;

    // Fetch products count for total count
    const totalProductsCount: number = await ProductModel.countDocuments(filter);

    // Calculate total pages
    const totalPages: number = Math.ceil(totalProductsCount / limit);

    // Fetch products from the database based on the filter and pagination
    const products: ProductDocument[] = await ProductModel.find(filter)
      .skip(skip)
      .limit(limit);

    // Return the fetched products along with total pages and current page
    const productsWithCategoryInfo = await Promise.all(products.map(async product => {
      const category = await CategoryModel.findById(product.category);
      return {
        _id: product._id,
        name: product.name,
        description: product.description,
        price: product.price,
        category: category ? { _id: category._id, name: category.name } : null
      };
    }));

    // If product ID is provided, return only that product
    if (id) {
      let validId:any = id;
      if (!mongoose.Types.ObjectId.isValid(validId)) {
        return res.error('Invalid product ID', 400);
      }

      const product = productsWithCategoryInfo.find(prod => prod._id.toString() === id);
      if (!product) {
        return res.status(404).json({ message: 'Product not found' });
      }
      return res.success(product, 'Products fetched successfully');
    }

    res.success({ products: productsWithCategoryInfo, totalPages, currentPage: page }, 'Products fetched successfully');
  } catch (error) {
    console.error(error);
    res.error('Server error', 500);
  }
};
export const updateProduct = async (req: Request, res: Response) => {
  try {
    // Check for validation errors
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const productId = req.params.productId;
    const { name, description, price, category } = req.body;

    // Check if the product ID is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(productId)) {
      return res.error('Invalid product ID', 400);
    }

    // Check if the product exists
    const existingProduct = await ProductModel.findById(productId);
    if (!existingProduct) {
      return res.error('Product not found', 404);
    }

    // Update the product details
    existingProduct.name = name;
    existingProduct.description = description;
    existingProduct.price = price;
    existingProduct.category = category;

    // Save the updated product to the database
    const updatedProduct = await existingProduct.save();

    // Respond with success and updated product data
    return res.success(updatedProduct, 'Product updated successfully');
  } catch (error) {
    console.error(error);
    return res.error('Server error', 500);
  }
};


export const deleteProduct = async (req: Request, res: Response) => {
    try {
        const productId = req.params.productId;

        // Check if the product ID is a valid ObjectId
        if (!mongoose.Types.ObjectId.isValid(productId)) {
            return res.error('Invalid product ID', 400);
        }

        // Check if the product exists
        const existingProduct = await ProductModel.findById(productId);
        if (!existingProduct) {
            return res.error('Product not found', 404);
        }

        // Delete the product
        await ProductModel.findByIdAndDelete(productId);

        // Respond with success message
        res.success({}, 'Product deleted successfully');
    } catch (error) {
        console.error(error);
        res.error('Server error', 500);
    }
};