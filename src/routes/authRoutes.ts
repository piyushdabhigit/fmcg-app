import express from 'express';
import { check } from 'express-validator';
import { registerUser, loginUser } from '../controllers/authController';

const router = express.Router();

// Register User
router.post(
  '/auth/register',
  [
    check('username', 'Username is required').not().isEmpty(),
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Please enter a password with 6 or more characters').isLength({ min: 6 }),
  ],
  registerUser
);

// Login
router.post('/auth/login', [
    check('email').isEmail().withMessage('Please enter a valid email'),
    check('password').isLength({ min: 6 }).withMessage('Password must be at least 6 characters long'),
], loginUser);

export default router;
