import mongoose, { Document, Schema } from 'mongoose';

export interface Category {
  name: string;
  description: string;
}

export interface CategoryDocument extends Category, Document {}

const categorySchema = new Schema<CategoryDocument>({
  name: { type: String, required: true },
  description: { type: String },
}, { timestamps: true }); // Automatically adds createdAt and updatedAt fields

const CategoryModel = mongoose.model<CategoryDocument>('Category', categorySchema);

export default CategoryModel;
