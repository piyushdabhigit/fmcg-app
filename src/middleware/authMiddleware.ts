import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

declare global {
  namespace Express {
    interface Request {
      userId?: string;
      user?: any; // Define 'user' property
    }
  }
}
// Define the structure of the JWT payload
interface TokenPayload {
  userId: string;
  username: string;
  role: string;
  
}

// Function to generate JWT token with payload
export const generateToken = (payload: TokenPayload): string => {
  return jwt.sign(payload, process.env.JWT_SECRET as string, { expiresIn: '1h' });
};

// Authentication middleware
export const authenticateUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    // Verify authorization token
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
      return res.error('Authorization token is missing', 401);
    }
    
    const decodedToken: any = jwt.verify(token, process.env.JWT_SECRET as string);
    
    req.userId = decodedToken.userId;
    req.user = decodedToken; 
    
    next();
  } catch (error) {
    console.error(error);
    return res.error('Invalid or expired token', 401);
  }
};
