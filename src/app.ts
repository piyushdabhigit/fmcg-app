import cors from 'cors';
import express, { Application } from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import dotenv from 'dotenv';
import connectDB from './config/database';
import responseBuilder from './middleware/responseBuilder';
import errorMiddleware from './middleware/errorMiddleware';
import userRoutes from './routes/userRoutes';
import authRoutes from './routes/authRoutes';
import categoryRoutes from './routes/categoryRoutes';
import productRoutes from './routes/productRoutes';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json'; // Import the Swagger JSON file


// Load environment variables from .env file
dotenv.config();

// Initialize Express app
const app: Application = express();
const PORT = process.env.PORT || 3000;
const MONGODB_URI = process.env.MONGODB_URI as string; // MongoDB URI from environment variable

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(morgan('dev'));
app.use(responseBuilder); // Apply the responseBuilder middleware before any routes


// Serve Swagger UI
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

connectDB(MONGODB_URI); // Call the connectDB function to establish MongoDB connection

// Routes
app.use('/api/', authRoutes);
app.use('/api/', userRoutes);
app.use('/api/', categoryRoutes);
app.use('/api/', productRoutes);

// Error handling middleware
app.use(errorMiddleware);

// Start server
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
