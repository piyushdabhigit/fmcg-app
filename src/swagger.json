{
  "openapi": "3.0.0",
  "info": {
    "title": "Backend Coding Challenge API",
    "description": "API documentation for the backend coding challenge",
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "http://localhost:3000/api"
    }
  ],
  "components": {
    "securitySchemes": {
      "BearerAuth": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    },
    "schemas": {
      "UserRegistration": {
        "type": "object",
        "properties": {
          "username": {
            "type": "string",
            "description": "Username of the user"
          },
          "email": {
            "type": "string",
            "format": "email",
            "description": "Email address of the user"
          },
          "password": {
            "type": "string",
            "format": "password",
            "description": "Password of the user"
          }
        }
      },
      "UserCredentials": {
        "type": "object",
        "properties": {
          "email": {
            "type": "string",
            "format": "email",
            "description": "Email address of the user"
          },
          "password": {
            "type": "string",
            "format": "password",
            "description": "Password of the user"
          }
        }
      },
      "UserUpdate": {
        "type": "object",
        "properties": {
          "username": {
            "type": "string",
            "description": "Updated username of the user"
          },
          "email": {
            "type": "string",
            "format": "email",
            "description": "Updated email address of the user"
          },
          "password": {
            "type": "string",
            "format": "password",
            "description": "Updated password of the user"
          }
        }
      },
      "Category": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "Name of the category"
          },
          "description": {
            "type": "string",
            "description": "Description of the category"
          }
        }
      },
      "Product": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "Name of the product"
          },
          "description": {
            "type": "string",
            "description": "Description of the product"
          },
          "price": {
            "type": "number",
            "description": "Price of the product"
          },
          "category": {
            "type": "string",
            "description": "Category ID of the product"
          },
          "upc": {
            "type": "string",
            "description": "UPC of the product"
          }
        }
      }
    }
  },
  "security": [
    {
      "BearerAuth": []
    }
  ],
  "paths": {
    "/auth/register": {
      "post": {
        "summary": "Register a new user",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserRegistration"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "User registered successfully"
          },
          "400": {
            "description": "Invalid request body"
          }
        }
      }
    },
    "/auth/login": {
      "post": {
        "summary": "Login with credentials",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserCredentials"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Logged in successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        }
      }
    },
    "/user": {
      
        "get": {
          "summary": "Get all users",
          "parameters": [
            {
              "in": "query",
              "name": "page",
              "schema": {
                "type": "integer"
              },
              "description": "Page number for pagination (default: 1)"
            },
            {
              "in": "query",
              "name": "limit",
              "schema": {
                "type": "integer"
              },
              "description": "Limit of items per page (default: 10)"
            }
          ],
          "responses": {
            "200": {
              "description": "List of users"
            }
          }
        }
      
      
    },
    "/user/{userId}": {
      "get": {
        "summary": "Get user details by ID",
        "parameters": [
          {
            "in": "path",
            "name": "userId",
            "required": true,
            "description": "ID of the user",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "User details"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "put": {
        "summary": "Update user details by ID",
        "parameters": [
          {
            "in": "path",
            "name": "userId",
            "required": true,
            "description": "ID of the user",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserUpdate"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "User details updated successfully"
          },
          "400": {
            "description": "Invalid request body"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "delete": {
        "summary": "Delete a user by ID",
        "parameters": [
          {
            "in": "path",
            "name": "userId",
            "required": true,
            "description": "ID of the user",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "User deleted successfully"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/categories": {
      "get": {
        "summary": "Get categories",
        "parameters": [
          {
            "in": "query",
            "name": "page",
            "schema": {
              "type": "integer"
            },
            "description": "Page number for pagination (default: 1)"
          },
          {
            "in": "query",
            "name": "limit",
            "schema": {
              "type": "integer"
            },
            "description": "Limit of items per page (default: 10)"
          }
        ],
        "responses": {
          "200": {
            "description": "List of categories"
          }
        }
      },
      "post": {
        "summary": "Create a new category",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Category"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Category created successfully"
          },
          "400": {
            "description": "Invalid request body"
          }
        }
      }
    },
    "/categories/{categoryId}": {
      "put": {
        "summary": "Update a category by ID",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "required": true,
            "description": "ID of the category",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Category"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Category updated successfully"
          },
          "400": {
            "description": "Invalid request body"
          },
          "404": {
            "description": "Category not found"
          }
        }
      },
      "delete": {
        "summary": "Delete a category by ID",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "required": true,
            "description": "ID of the category",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Category deleted successfully"
          },
          "404": {
            "description": "Category not found"
          }
        }
      }
    },
    "/product": {
      "get": {
        "summary": "Get products",
        "parameters": [
          {
            "in": "query",
            "name": "page",
            "schema": {
              "type": "integer"
            },
            "description": "Page number for pagination (default: 1)"
          },
          {
            "in": "query",
            "name": "limit",
            "schema": {
              "type": "integer"
            },
            "description": "Limit of items per page (default: 10)"
          },
          {
            "in": "query",
            "name": "name",
            "schema": {
              "type": "string"
            },
            "description": "Name of the product to filter"
          },
          {
            "in": "query",
            "name": "upc",
            "schema": {
              "type": "string"
            },
            "description": "UPC of the product to filter"
          },
          {
            "in": "query",
            "name": "categoryName",
            "schema": {
              "type": "string"
            },
            "description": "Name of the category to filter"
          },
          {
            "in": "query",
            "name": "id",
            "schema": {
              "type": "string"
            },
            "description": "ID of the product to filter"
          }
        ],
        "responses": {
          "200": {
            "description": "List of products"
          }
        }
      },
      
      "post": {
        "summary": "Create a new product",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Product"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Product created successfully"
          },
          "400": {
            "description": "Invalid request body"
          }
        }
      }
    },
    "/product/{productId}": {
      "put": {
        "summary": "Update a product by ID",
        "parameters": [
          {
            "in": "path",
            "name": "productId",
            "required": true,
            "description": "ID of the product",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Product"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Product updated successfully"
          },
          "400": {
            "description": "Invalid request body"
          },
          "404": {
            "description": "Product not found"
          }
        }
      },
      "delete": {
        "summary": "Delete a product by ID",
        "parameters": [
          {
            "in": "path",
            "name": "productId",
            "required": true,
            "description": "ID of the product",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Product deleted successfully"
          },
          "404": {
            "description": "Product not found"
          }
        }
      }
    }
  }
}
