# Backend Coding Challenge

This project is for the backend coding challenge. It implements a small server application with basic functionalities such as authentication, role management, CRUD operations, etc.

## Getting Started

1. Clone the repository.
2. Install dependencies: `npm install`
3. Set up environment variables by creating a `.env` file based on the `.env.example` file.
4. Start the server: `npm start`

## Endpoints

### Authentication

- **Register User**
- **POST** `/api/auth/register`
- **Body:**
 ```json
 {
     "username":"shwesh",
     "email":"shwesh@gmail.com",
     "password":"123abc",
     "role":"admin"
 }
 ```

- **Login User**
- **POST** `/api/auth/login`
- **Body:**
 ```json
 { 
     "email":"shwesh@gmail.com",
     "password":"123abc"
 }
 ```

### User Management

- **Get User**
- **GET** `/api/user?page=1&limit=1`

- **Get User By ID**
- **GET** `/api/user/663f0bde9a68d1eb1ba2a8a4`

- **Update User**
- **PUT** `/api/user/663f0bde9a68d1eab1ba2a8a4`
- **Body:**
 ```json
 {
     "username":"hye",
     "email":"admin@gmail.com",
     "password":"123456789"
 }
 ```

- **Delete User**
- **DELETE** `/api/user/663f9c46f3c4c37a2ffb05b2`

### Category Management

- **Get Categories**
- **GET** `/api/categories?page=1&limit=10`

- **Create Category**
- **POST** `/api/categories`
- **Body:**
 ```json
 { 
     "name":"a1",
     "description":"yukuuu"
 }
 ```

- **Update Category**
- **PUT** `/api/categories/663f5bd44deb05aa6c1506aa`
- **Body:**
 ```json
 { 
     "name":"okayyy",
     "description":"yeah"
 }
 ```

- **Delete Category**
- **DELETE** `/api/categories/663f5bd44deb05aa6c1506aa`

### Product Management

- **Get Products**
- **GET** `/api/product?page=1&limit=10&name=&upc=&categoryName=&id=`

- **Create Product**
- **POST** `/api/product`
- **Body:**
 ```json
 {
   "name": "Hey",
   "description": "Product Description",
   "price": 100,
   "category": "663fa9052fa0aeff0e98e67c",
   "upc": "ab456c"
 }
 ```

- **Update Product**
- **PUT** `/api/product/6641a781d676b88f262fcb5b`
- **Body:**
 ```json
 {
   "name": "Edit Namae",
   "description": "Test Description",
   "price": 100,
   "category": "663fa9052fa0aeff0e98e67c"
 }
 ```

- **Delete Product**
- **DELETE** `/api/product/6641a781d676b88f262fcb5b`


# Scripts
- **start**: `npm-run-all -p build watch:server`
- **build**: `tsc`
- **watch:server**: `nodemon dist/app.js`
